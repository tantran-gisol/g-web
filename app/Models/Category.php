<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
		'name', 'featured_image', 'sub_featured_image', 'parent_id'
	];

	public function posts()
  {
    return $this->belongsToMany(Post::class);
  }

	public function children()
  {
    return $this->hasMany(Category::class, 'parent_id', 'id');
  }

  public function getParentCategories(){
    $query = $this->query();
    $query->where('parent_id', null);
    return $query->get();
  }

  public function getFeatureCategories($catID='', $number=3){
    if(empty($catID)){
      return false;
    }
    $query = $this->query();
    $query->where('parent_id', $catID);
    return $query->limit($number)->get();
  }

  public function getSeriesCategories($catID='', $number=3){
    if(empty($catID)){
      return false;
    }
    $query = $this->query();
    $query->where('parent_id', $catID);
    return $query->limit($number)->get();
  }
}