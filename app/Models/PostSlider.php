<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostSlider extends Model
{
	protected $fillable = [
		'post_id', 'slide_images'
	];
	public function post()
  {
    return $this->belongsTo(Post::class);
  }

}