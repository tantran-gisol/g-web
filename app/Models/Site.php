<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'title', 'description', 'cover_image_url', 'user_id'
    ];
    public function owner()
    {
        return $this->belongsTo(User::class);
    }
    public function members()
    {
        return $this->hasMany(User::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}