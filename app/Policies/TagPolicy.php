<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\AbstractPolicy\AbstractPolicy;

class TagPolicy extends AbstractPolicy {
    protected $model = 'tag';

    public function index(User $user) {
        return $user->ability('admin.' . $this->model . '.index');
    }

    public function edit(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.edit');
    }

    public function create(User $user) {

        return $user->ability('admin.' . $this->model . '.create');
    }

    public function update(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.update');
    }

    public function reorder(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.reorder');
    }

    public function store(User $user) {
        return $user->ability('admin.' . $this->model . '.store');
    }

    public function destroy(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.destroy');
    }
}
