<?php

namespace App\Http\ViewComposers;

use App\Models\Setting;
use Illuminate\View\View;

class SettingsComposer
{
    /**
     * The user repository implementation.
     *
     */
    protected $settingModel;

    /**
     * Create a new profile composer.
     *
     * @param MenuService $menuService
     */
    public function __construct(Setting $settingModel)
    {
        // Dependencies automatically resolved by service container...
        $this->settingModel = $settingModel;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $settings = $this->settingModel->getSettingsData();
        $view->with('settings', $settings);
    }

}