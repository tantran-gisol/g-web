<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Setting;
use Helper;

class HomeController extends Controller
{
    protected $postModel;
    protected $categoryModel;
    protected $settingModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Post $postModel, Category $categoryModel, Setting $settingModel )
    {
        $this->postModel = $postModel;
        $this->categoryModel = $categoryModel;
        $this->settingModel = $settingModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $sliderPosts = $this->postModel->getSliderPosts(1, 5);
        $topPosts = $this->postModel->getTopPosts(5);
        $postsPC = $this->postModel->getPosts($request, 1, 9);
        $postsSP = $this->postModel->getPosts($request, 1, 6);
        $featureCatId = $this->settingModel->getSettingData('siderbar_feature_cat_id');
        $seriesCatId = $this->settingModel->getSettingData('siderbar_series_cat_id');
        $featureCategories = $this->categoryModel->getFeatureCategories($featureCatId);
        $seriesCategories = $this->categoryModel->getSeriesCategories($seriesCatId);
        $instaFeeds = Helper::get_instagram_feed();
        return view('frontend.templates.home.index')->with([
            'sliderPosts' => $sliderPosts,
            'topPosts' => $topPosts,
            'postsPC' => $postsPC,
            'postsSP' => $postsSP,
            'featureCategories' => $featureCategories,
            'seriesCategories' => $seriesCategories,
            'instaFeeds' => $instaFeeds
        ]);
    }
    
}
