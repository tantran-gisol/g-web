<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Lienhe;
use App\Http\Controllers\Controller;

class LienheController extends Controller
{
	protected $lienhe;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Lienhe $lienhe)
    {
        $this->lienhe = $lienhe;
    }

	public function index()
	{
        return view('frontend.templates.lienhe.index');
    }
    // Store Contact Form data
    public function Lienhe(Request $request) {

        // Form validation
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject'=> 'required',
            'message' => 'required'
         ]);
        //  Store data in database
        Lienhe::create($request->all());

        // 
        return back()->with('Đã gửi', 'Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi');
    } 
}
