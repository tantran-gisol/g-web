<?php

return [
	'sizes_generate' =>[
		'original' => [
			'width' => null,
			'height' => null
		],
		'xlarge' => [
			'width' =>1280,
			'height' => null
		],
		'large' => [
			'width' =>640,
			'height' => null
		],
		'medium' => [
			'width' =>320,
			'height' => null
		],
		'small' => [
			'width' =>160,
			'height' => null
		],
		'thumbnail' => [
			'width' =>90,
			'height' => 90
		]
	],
	'adiva_roles' => [
		'admin' => [
			'admin.manager.index',
			// user
			'admin.user.index',
			'admin.user.indexofficialwriter',
			'admin.user.edit',
			'admin.user.create',
			'admin.user.store',
			'admin.user.update',
			'admin.user.update_avatar',
			'admin.user.destroy',
			//post
			'admin.post.index',
			'admin.post.indexBySite',
			'admin.post.edit',
			'admin.post.create',
			'admin.post.store',
			'admin.post.update',
			'admin.post.destroy',
			'frontend.post.preview',
			//post_view
			'admin.post_view.index',
			'admin.post_view.edit',
			'admin.post_view.create',
			'admin.post_view.store',
			'admin.post_view.update',
			'admin.post_view.destroy',
			//tag
			'admin.tag.index',
			'admin.tag.edit',
			'admin.tag.create',
			'admin.tag.store',
			'admin.tag.update',
			'admin.tag.reorder',
			'admin.tag.destroy',
			//category
			'admin.category.index',
			'admin.category.edit',
			'admin.category.create',
			'admin.category.store',
			'admin.category.update',
			'admin.category.reorder',
			'admin.category.destroy',
			//menu_tag
			'admin.menu_tag.index',
			'admin.menu_tag.edit',
			'admin.menu_tag.create',
			'admin.menu_tag.store',
			'admin.menu_tag.update',
			'admin.menu_tag.destroy',
			//menu_tag
			'admin.menu.index',
			'admin.menu.edit',
			'admin.menu.create',
			'admin.menu.store',
			'admin.menu.update',
			'admin.menu.destroy',
			//media
			'admin.media.index',
			'admin.media.edit',
			'admin.media.create',
			'admin.media.store',
			'admin.media.update',
			'admin.media.destroy',
			//inquiry
			'admin.inquiry.index',
			'admin.inquiry.edit',
			'admin.inquiry.create',
			'admin.inquiry.store',
			'admin.inquiry.update',
			'admin.inquiry.destroy',
			//site
			'admin.site.index',
			'admin.site.edit',
			'admin.site.create',
			'admin.site.store',
			'admin.site.update',
			'admin.site.destroy',
			//setting
			'admin.setting.index',
			'admin.setting.edit',
			'admin.setting.create',
			'admin.setting.store',
			'admin.setting.update',
			'admin.setting.destroy',
			//page
			'admin.page.index',
			'admin.page.edit',
			'admin.page.create',
			'admin.page.store',
			'admin.page.update',
			'admin.page.destroy',
			'admin.page.change_status',
			//supportemail
			'admin.supportemail.index',
			'admin.supportemail.edit',
			'admin.supportemail.create',
			'admin.supportemail.store',
			'admin.supportemail.update',
			'admin.supportemail.destroy',
			//supportemail
			'admin.vote.index',
			'admin.vote.edit',
			'admin.vote.create',
			'admin.vote.store',
			'admin.vote.update',
			'admin.vote.destroy',
		],

		'writer' => [
			'admin.manager.index',
			//user
			'admin.user.edit',
			'admin.user.update',
			'admin.user.update_avatar',
			//post
			'admin.post.indexBySite',
			'admin.post.create',
			'admin.post.store',
			'admin.post.edit',
			'admin.post.update',
			'admin.post.destroy',
			'frontend.post.preview',
		],
	],
	'page_defaults' => [
		'search',
		'inquiry',
		'privacy-policy',
		'terms-of-service',
		'feature',
		'series'
	],
	'menu_locations' => [
		'footer_menu_1' => 'タグ',
		'footer_menu_2' => '連載',
		'footer_menu_3' => 'ヘルプ'
	]
];