@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>Cài đặt phương tiện</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')

		<form action="{{ route('admin.setting.update',['redirect' => route('admin.setting.widget')]) }}" method="POST" class="media-edit-form" style="margin: 1rem 0;">
			{{-- <section>
				<div class="nav-tab-1-title">
					<p>Cài đặt nhúng</p>
				</div>
				<div class="media-edit">
					<form action="{{ route('admin.setting.update',['redirect' => route('admin.setting.widget')]) }}" method="POST" class="media-edit-form" style="margin: 1rem 0;">

						<div class="form-group row" style="padding: 1rem 0 2rem 0;">
							<div class="col-lg-2 col-md-3 text-left text-md-right">
								<label>Chiều rộng </label>
							</div>
							<div class="col-12 col-lg-10 col-md-9">
								<div class="d-flex" style="width: 80%; background: #fff; padding: 0.5rem;">
									<div>
										<i class="fas fa-info-circle" style="color: #888;"></i>
									</div>
									
									<div style="color: #888;">
										<p style="margin-bottom: 1rem;">Unit (PX)</p>
									</div>
								</div>
								<input type="text" class="form-control" placeholder="" name="embed_width" value="{{ (array_key_exists('embed_width', $settings)) ? $settings['embed_width'] : '' }}" required="" style="font-size: 12px;" />
							</div>
						</div>
						<div class="form-group row" style="padding: 1rem 0 2rem 0;">
							<div class="col-lg-2 col-md-3 text-left text-md-right">
								<label>Chiều cao </label>
							</div>
							<div class="col-12 col-lg-10 col-md-9">
								<div class="d-flex" style="width: 80%; background: #fff; padding: 0.5rem;">
									<div>
										<i class="fas fa-info-circle" style="color: #888;"></i>
									</div>
									
									<div style="color: #888;">
										<p style="margin-bottom: 1rem;">Unit (PX)</p>
									</div>
								</div>
								<input type="text" class="form-control" placeholder="" name="embed_height" value="{{ (array_key_exists('embed_height', $settings)) ? $settings['embed_height'] : '' }}" required="" style="font-size: 12px;" />
							</div>
						</div>
				</div>
			</section> --}}
			<section>
				<div>
					<p>Cài đặt tiện ích </p>
				</div>
				<div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>Hình ảnh</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<div class="img-wrap" style="width: 200px; height: 100px; background: url('{{ (array_key_exists('widget_image', $settings)) ? $settings['widget_image'] : '' }}'); margin: 1rem 0 .5rem 0;" data-image-input-preview="widget_image">
							</div>
							<input type="text" class="form-control" placeholder="" name="widget_image" value="{{ (array_key_exists('widget_image', $settings)) ? $settings['widget_image'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">
							<button type="button" class="btn btn-no-radius open-library" data-input-name="widget_image" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>Mã nhúng</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<textarea class="form-control" readonly=""><iframe src="{{ route('frontend.widget.embed') }}" frameborder="0" style="width: 100%; height: 500px;"></iframe></textarea>
						</div>
					</div>
				</div>
			</section>
			<button class="btn btn-no-radius" style="background: #111; color: #fff;">Cập nhật</button>
			{{ csrf_field() }}
		</form>
	</section>
	<!-----x----- main -----x----->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!--x-- Library modal -->
@endsection