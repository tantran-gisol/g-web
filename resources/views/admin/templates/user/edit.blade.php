@extends('admin.layouts.app')
@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="change-avatar">
			<div>
				<h3>Thông tin quản trị viên</h3>
				<p>Chỉnh sửa thông tin quản trị viên.</p>
				@include('admin.parts.alert')
			</div>
			
			<div class="change-avatar-img-bg" style="margin-top: 57px;">
				<div class="change-avatar-img">
					<form action="{{ route('admin.user.update_avatar', $user->id) }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<img id="changeimg" src="{{ !empty($user->avatar) ? Helper::getImageUrl($user->avatar) : asset('image/no-img.png') }}" style="height: 116px;width: 38%; object-fit: cover;"><br>
						<div class="row" style="margin-top: 16px;">
							<button style="height: 35px; margin-left: 5px; padding: 5px 10px;" disabled>
								<input type="file" name="file" id="file" class="inputfile">
								<label for="file" style="margin: 0; width: 100%;">Chọn tệp tin</label>
							</button>
							<p style="font-size: 12px;margin-top:2%;margin-left:5px;">Không được chọn</p>
						</div>
						<!-- <div ></div> -->
						<button style="width: 85px;height: 31px; font-size: 13px;margin-top: 10px;" class="btn-change-avatar btn btn-no-radius">Cập nhật</button>
					</form>
				</div>
			</div>
		</div>		
		<div class="admin-edit-info" style="margin-top: 30px;">
			<form class="edit-info" action="{{ route('admin.user.update', $user->id) }}" method="post">
				{{ csrf_field() }}
				<div class="form-group edit-info-email row" style="background: #f8f8f8; padding-top: 1rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Địa chỉ email</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="email" class="form-control" name="email" value="{{ $user->email }}" required="" style="font-size: 12px;" />
						<div class="form-check">
							<!-- <input type="checkbox" class="form-check-input active" name="receive_notification" id="receive_notification" value="1" {{ $user->receive_notification ? 'checked' : '' }} />
							<label class="form-check-label" for="receive_notification">メールで管理者通知を受け取る</label> -->
						</div>
					</div>
				</div>
				<div class="form-group edit-info-warning row">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Mật khẩu</label>
					</div>
					<div class="col-md-9 col-12">
						<div class="text-warning"><p style="margin-bottom: 0;"><i class="fas fa-exclamation-triangle"></i>Nếu bạn không muốn thay đổi mật khẩu, hãy để trống.</p></div>
					</div>
				</div>
				<div class="form-group edit-info-pass row">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12"></div>
					<div class="edit-pass col-md-9 col-12">
						<div>
							<p style="margin-bottom:5px;">Mật khẩu hiện tại</p>
							<input type="password" class="old-pass form-control" name="current_password" value="{{ old('current_password') }}" style="font-size: 12px; width: calc(100% - 1rem);">
						</div>
						<div style="margin-top: 1rem;">
							<p style="margin-bottom:5px;">Mật khẩu mới</p>
							<input type="password" class="new-pass form-control" name="password" value="{{ old('password') }}" style="font-size: 12px; width: calc(100% - 1rem);">
						</div>
						<div class="d-flex mt-3 mb-3">
							<i class="fas fa-info-circle" style="color: #888;"></i>
							<label class="ml-2" style="width: 59%;margin-left: 17px">Nhập mật khẩu dưới dạng chuỗi ký tự chữ và số một byte từ 8 đến 64 ký tự. Phải chứa tất cả các ký hiệu viết hoa và viết thường.</label>
						</div>
						<div>
							<p style="margin-bottom:5px;">Xác nhận mật khẩu mới</p>
							<input type="password" class="confirm-new-pass form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" style="font-size: 12px; width: calc(100% - 1rem);">
						</div>
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2.5rem 0; background: #f8f8f8; margin-top: 2rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Tên</label>
					</div>
					<div class="col-md-9 col-12" style="margin-bottom: 10px;">
						<input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" style="font-size: 12px;" />
					</div>
				</div>
				<!-- <div class="form-group edit-info-sns row"style="margin-top:18px !important;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>外部SNS連携</label>
					</div>
					<div class="col-md-9 col-12" style="margin-top: 7px;">
						<div class="d-flex">
							<a href="#" class="btn btn-outline-dark mr-2" style="font-size: 12px;">Facebookに接続</a>
							<a href="#" class="btn btn-outline-dark" style="font-size: 12px;">Twitterに接続</a>
						</div>
					</div>
				</div> -->
				<div class="form-group row" style="background: #f8f8f8;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12" style="padding-top: 1rem;">
						<label>Hồ sơ</label>
					</div>
					<div class="col-md-9 col-12" style="padding-top: 1rem;">
						<textarea type="text" class="edit-your-info form-control" rows="8" name="writer_profile_html" style="font-size: 12px;">{{ old('writer_profile_html', $user->writer_profile_html) }}</textarea>
						
						<div class="d-flex" style="margin-left: 0; padding: .5rem 0; background: #ffffff; width: 75%; margin-top: 18px;margin-bottom: 10px">
							<i class="fas fa-info-circle" style="margin-left: .5rem; "></i>
							<label class="ml-2">Các phương tiện truyền thông<span style="color: #c43638;">Trang người dùng</span>Nó là một văn bản giới thiệu được mô tả bằng HTML có thể được sử dụng</label>
						</div>
					</div>
				</div>
				@if(Auth::user()->role->slug == 'admin' && (Auth::user()->site->user_id !== $user->id) )
					<div class="form-group row">
						<div class="label-admin-edit col-lg-2 col-md-3 col-12">
							<label style="margin-top: 8px;">Thẩm quyền</label>
						</div>
						<div class="col-md-9 col-12">
							<select name="role_id" style=" width: 215px;" required="">
								<option value="">Thẩm quyền</option>
								@if(count($roles))
									@foreach($roles as $role)
										<option value="{{ $role->id }}" {{ (old('role_id', $user->role_id) == $role->id ) ? 'selected' : '' }}>{{ $role->name }}</option>
									@endforeach
								@endif
							</select>

							<!-- <div class="dropdown-warning font-weight-bold">
								<input type="checkbox" id="disable_article_publishing" name="disable_article_publishing" value="1" {{ $user->disable_article_publishing ? 'checked' : '' }} />
								<label for="disable_article_publishing">記事の公開を不可にする</label>
							</div> -->
							<!-- <div class="d-flex" style="margin-top:30px; ">
								<i class="fas fa-info-circle"></i>
								<label class="ml-2" style="margin-left: 17px">「記事の公開を不可にする」をチェックすると、ユーザーは下きき状態の記事だ<br>けを扱える状態になります。<br>この設定はユーザーの権限が「記事作成者」の場合にのみ指定可能です。</label>
							</div> -->
						</div>
					</div>
				@endif
				<div style="margin-top: 70px;">					
				@if(Auth::user()->role->slug == 'admin' && (Auth::user()->site->user_id != $user->id) )
					<a href="{{ route('admin.user.index') }}" class="btn-back btn btn-no-radius" style="font-size: 12px; width: 100px; line-height: 28px;">
						<i class="fas fa-arrow-left"></i>
						Quay lại
					</a>
				@endif
					<button class="btn-submit btn btn-no-radius" style="font-size: 12px; width: 100px;">Cập nhật</button>
				</div>
			</form>
		</div>
	</section>
	<!-----x----- admin-edit main section -----x----->
@endsection