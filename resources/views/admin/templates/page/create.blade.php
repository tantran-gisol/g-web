@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="post-edit">
			<h3 class="font-weight-bold">Bài đăng</h3>
			<p class="font-weight-bold">Tạo và chỉnh sửa trang.</p>
			@include('admin.parts.alert')
		</div>

		<div class="admin-post-news">
			<form class="post-news" action="{{ route('admin.page.store') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Tiêu đề</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="text" class="form-control" name="title" value="{{ old('title') }}" required="">
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Liên kết</label>
					</div>
					<div class="col-md-9 col-12">
						<span>{{ route('frontend.home.index') }}/_p/ </span><input type="text" class="form-control d-inline w-auto" name="slug" value="{{ old('slug') }}" />
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Ảnh bìa</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="file" name="file" accept="image/x-png,image/jpeg" />
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>Nội dung</label>
					</div>
					<div class="col-md-9 col-12 page-content">
						<textarea id="editor" name="content" style="width: 100%;">{{ old('content') }}</textarea>
					</div>
				</div>

				<div>
					<div class="col-11" style="margin-top: 1rem;">
						<div class="float-right">
							<a href="{{ route('admin.setting.fixedpage') }}" class="btn btn-no-radius" style="font-size: 12px; width: 100px; line-height: 26px; color: #fff; background: #111111; margin-right: 10px;">
								<i class="fas fa-arrow-left" style="color: #fff"></i>
								Hủy bỏ
							</a>
							<button class="btn-submit btn btn-no-radius" style=" width: 100px;">
								<i class="fas fa-check" style="color: #ffffff;"></i>
								Lưu
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
	<!-----x----- admin-edit main section -----x----->
	<style>
		.ck-content.ck-editor__editable { 
	    min-height: 400px; 
		}
		.page-content h2 {
			font-size: 137.5%;
			font-weight: 700;
			margin-top: 2em;
	    padding: 7px 7px 4px;
	    border-top: 4px double #0050aa;
	    border-bottom: 4px double #0050aa;
		}
		.page-content h3 {
			font-size: 112.5%;
	    margin: 16px 0 8px 0;
	    border-left: 5px solid #0050aa;
	    line-height: 1.25;
	    padding: .15em 0 .1em 0.65em;
		}
		.page-content h4 {
			font-size: 16px;
	    font-weight: 600;
	    padding-left: 8px;
	    padding-bottom: 5px;
	    border-bottom: 1px dotted;
	    margin-bottom: 10px;
		}
	</style>
@endsection
@section('footer_js')
	<script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/ckeditor.js"></script>
	<script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/translations/ja.js"></script>
	<script src="{{ asset('js/admin/page-content-editor.js') }}"></script>
@endsection