<div class="modal-header">
  <h3>Media</h3>
</div>
<div class="modal-body">
  <div class="tab-content">
    <div id="post-media">
      <div class="row list-post-medias">
        @if(count($medias))
          @php
            $media_image_ids = array_column($media_images, 'id');
          @endphp
          @foreach($medias as $media)
            <div class="col-2">
              <a href="#" class="media-item {{ (in_array($media->id, $media_image_ids)) ? 'active' : '' }}" data-id="{{ $media->id }}" data-url="{{ Helper::getMediaUrl($media, 'original') }}" title="{{ $media->name }}">
                <i class="fa fa-check"></i>
                <img src="{{ Helper::getMediaUrl($media, 'thumbnail') }}" alt="" />
              </a>
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <input type="hidden" class="input-media-images" value="{{ json_encode($media_images) }}" />
  <a type="button" class="btn btn-default btn-primary btn-update-media-images">Thêm</a>
  <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
</div>