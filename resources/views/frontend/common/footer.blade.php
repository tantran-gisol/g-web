<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Menu</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i><a href="{{ url('gioithieu') }}">GIỚI THIỆU</a></li>
              <li><i class="ion-ios-arrow-right"></i><a href="#">HỘI THẢO</a></li>
              <li><i class="ion-ios-arrow-right"></i><a href="#">THÔNG TIN NGÀNH HỌC</a></li>
              <li><i class="ion-ios-arrow-right"></i><a href="{{ url('tintuc') }}">TIN TỨC</a></li>
              <li><i class="ion-ios-arrow-right"></i><a href="{{ url('lienhe') }}">LIÊN HỆ</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>VĂN PHÒNG CHÍNH</h4>
            <p>
              319 C16 Lý Thường Kiệt, Phường 15, Quận 11, Tp.HCM <br>
              076 922 0162<br>
              demonhunterg@gmail.com<br>
              demonhunterp<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>NGÀNH HỌC</h4>
            <p>
              Điều dưỡng <br>
              Công nghệ thông tin<br>
              Nhà hàng – Khách sạn <br>
              Cơ khí – Kỹ thuật ôtô<br>
              Đầu bếp<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>ĐĂNG KÝ</h4>
            <p>Đăng ký để nhận được được thông tin mới nhất từ chúng tôi.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="GỬI">
            </form>
            <div class="social-links" style="margin-top: 10%">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Bản quyền thuộc về <strong>Thiết kế website </strong>.Gisol.vn
      </div>
      <div class="credits">
        Designed by <a href="https://bootstrapmade.com/">Gisol.vn</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>