@extends('frontend.layouts.app')
@section('content')
<main id="main" style="margin-top: 9%;height: 350px;">
		<div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s" style="margin-left:35%">
            <h4 class="title" style="text-align:left;color:#0071bb;font-size:20px;">Đăng nhập</h4>
            <div class="container mt-5" style="margin-top: 1.5rem !important;">
            <!-- Success message -->
                  @if(Session::has('success'))
                      <div class="alert alert-success">
                          {{Session::get('success')}}
                      </div>
                  @endif
              <form action="{{ route('frontend.dangnhap.index') }}" method="post" role="form" class="contactForm" style="margin-left:8%">
              @csrf
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control {{ $errors->has('name') ? 'error' : '' }}" name="name" id="name">
                  @if ($errors->has('name'))
                    <div class="error">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control {{ $errors->has('password') ? 'error' : '' }}" name="password" id="password">
                  @if ($errors->has('password'))
                    <div class="error">
                      {{ $errors->first('password') }}
                    </div>
                  @endif
                </div>

                <button type="submit" class="btn btn-lg btn-block btn-danger toggleResult" id="" style="background-color:rgb(0, 113, 187)">Đăng nhập</button>
            </form>
          </div>
        </div>
        </main>
@endsection          