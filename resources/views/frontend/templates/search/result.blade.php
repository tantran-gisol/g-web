@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main article details page content -->

		<div class="main-search-page-result">
			<div class="container">
				<div class="search-page-result-header row">
					<div class="col-12">
						<form action="{{ route('frontend.search.result') }}" method="GET">
							<div class="search-page-input">
								<button class="border-0 p-0 mr-3 bg-white"><i class="fas fa-search"></i></button>
								<input type="text" class="w-50 mw-75 border-0 " style="font-size: 1.25rem;" name="s" placeholder="Vui lòng nhập một từ khóa" value="{{ request()->get('s') }}" />
							</div>
						</form>
						<p>Kết quả tìm kiếm <span style="margin-left: 10px">{{ $posts->total() }}Vấn đề</span></p>
					</div>
				</div>
			
				<div class="line"></div>

				<div class="category-content row">
					
					<!-- left category content -->

					<div class="category-content-item-list col-lg-8 col-md-8 col-12">
						@if(count($posts))
							@foreach($posts as $post)
								<div class="category-content-item d-flex">
									<a href="{{ route('frontend.post.show', $post->slug) }}"><div class="category-content-item-img" style="width: 150px; height: 150px;">
										@if($post->featureImage)
											<img src="{{ Helper::getMediaUrl($post->featureImage, 'small') }}" />
										@else
											<img src="{{ Helper::getDefaultCover($post) }}" />
										@endif
									</div></a>

									<div class="category-item-text" style="margin-left: 0; font-size: 20px;">
										<div class="category-content-item-title font-weight-bold col-12">
											<a href="{{ route('frontend.post.show', $post->slug) }}">
												@if(!empty($post->vote_id) && array_sum($post->getVoteCountData()) > 0)
													<span class="vote-label">VOTE</span>
												@endif
												<span>{{ $post->title }}</span>
											</a>
										</div>
										@php
											$summary = Helper::getPostSummary($post->content);
										@endphp
										<div class="category-content-item-content col-12">
											@if(!empty($summary))
												{!! $summary !!}
											@else
												{!! strip_tags($post->content) !!}
											@endif
										</div>
										<div class="category-content-item-post col-12 d-none d-md-block">
											<div class="post-man-avatar d-flex">
												<a href="{{ route('frontend.user.show', $post->user) }}">
													@if($post->user->avatar)
														<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}">
													@else
														<img src="{{ asset('image/no-img.png') }}">
													@endif
												</a>
												<div style="padding: 8px 0; line-height: 1rem;">
													<div for="" class="post-man-name col-12">
														<a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a>
													</div>
													<div for="" class="post-man-date col-12">
														{{ date('Y-m-d', strtotime($post->created_at)) }}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@endif
						
						<!-- pagination -->
						{{ $posts->appends(request()->query())->links('frontend.parts.pagination') }}
						<!--x-- pagination -->
					</div>

					<!--x-- left category content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main article details page content --x-->

	</section>
@endsection