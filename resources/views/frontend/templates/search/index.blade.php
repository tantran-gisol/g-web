@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!---------- main article details page content ---------->

		<div class="main-search-page">
			<div class="container">
				<div class="search-page-header row">
					<div class="col-lg-6 col-md-6 col-12">
						<form action="{{ route('frontend.search.result') }}">
							<div class="search-page-input">
								<input type="text" class="search-input-search-page" name="s" placeholder="Vui lòng nhập một từ khóa">
								<button><i class="fas fa-search"></i></button>
							</div>
						</from>
					</div>
				</div>
			
				<div class="line"></div>

				<div class="category-content row">
					
					<!---------- left category content ---------->

					<div class="category-content-item-list col-lg-8 col-md-8 col-12">
						
					</div>

					<!-----x----- left category content -----x----->

					<!---------- right category content ---------->

					@include('frontend.parts.right_sidebar')

					<!-----x----- right category content -----x----->

				</div>
			</div>
		</div>

		<!-----x----- main article details page content -----x----->

	</section>
@endsection