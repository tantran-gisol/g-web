@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>
        <h3 class="section-header">DANH SÁCH TRƯỜNG NHẬT NGỮ</h3>
        <div class="row about-cols">

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('image/about-mission.jpg') }}" alt="" class="img-fluid">
                <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
              </div>
              <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2>
              <p>
                Trả lời phỏng vấn của cục xuất nhập cảnh (XNC) Nhật Bản là bước rất ...
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('image/about-plan.jpg') }}" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#">Điều kiện du học Nhật Bản 2019</a></h2>
              <p>
                I – ĐIỀU KIỆN DU HỌC NHẬT BẢN CHƯƠNG TRÌNH THÔNG THƯỜNG 1 – DU ...
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('image/about-vision.jpg') }}" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#">Hướng dẫn làm thủ tục nhập cảnh vào Nhật Bản</a></h2>
              <p>
                Thủ tục nhập cảnh vào Nhật bản là thủ tục bắt buộc phải làm đối với ...
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('image/about-vision.jpg') }}" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#">Những điều nên làm khi mới sang Nhật</a></h2>
              <p>
               Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong ...
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('image/about-vision.jpg') }}" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#"></a></h2>
              <p>
                Nguyên tắc chọn ngành đi du học Nhật Bản Trước hết, bạn cần ngồi lại ...
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('image/about-vision.jpg') }}" alt="" class="img-fluid">
              </div>
              <h2 class="title"><a href="#">Du học Nhật Bản cần bao nhiêu tiền và chi phí gì</a></h2>
              <p>
                I – TỔNG QUAN CHI PHÍ TRƯỚC VÀ SAU DU HỌC NHẬT BẢN 2019 Nhật ...
              </p>
            </div>
          </div>

        </div>

      </div>
    </section>
  </main>
@endsection