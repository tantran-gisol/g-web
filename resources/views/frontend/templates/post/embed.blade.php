<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<style type="text/css">
		body, html {
			margin: 0;
			padding: 0;
			font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
		}
		.post-embed {
			padding: 25px;
	    font-size: 14px;
	    font-weight: 400;
	    line-height: 1.5;
	    color: #82878c;
	    background: #fff;
	    border: 1px solid #e5e5e5;
	    box-shadow: 0 1px 1px rgba(0,0,0,.05);
	    overflow: auto;
	    zoom: 1;
		}
		.post-embed a {
	    color: #82878c;
    	text-decoration: none;
		}
		.post-embed p {
		  margin: 0;
		}
		.post-embed .embed-more {
		  color: #b4b9be;
		}
		.post-embed .embed-heading {
	    margin: 0 0 15px;
	    font-weight: 600;
	    font-size: 22px;
	    line-height: 1.3;
		}
		.post-embed .embed-heading a {
	    color: #32373c;
		}
		.post-embed .embed-featured-image {
	    margin-bottom: 20px;
		}
		.post-embed .embed-featured-image.square {
	    float: left;
	    max-width: 160px;
	    margin-right: 20px;
		}
		.post-embed .embed-featured-image img {
		  width: 100%;
		  height: auto;
		  border: none;
		}
		.post-embed .embed-footer {
		  display: table;
		  width: 100%;
		  margin-top: 30px;
		}
		.post-embed .embed-site-title {
		  font-weight: 600;
		  line-height: 25px;
		}
		.post-embed .embed-site-title a {
		  position: relative;
		  display: inline-block;
		  padding-left: 35px;
		}
		.post-embed .embed-site-icon {
		  position: absolute;
		  top: 50%;
		  left: 0;
		  -webkit-transform: translateY(-50%);
		  transform: translateY(-50%);
		  height: 25px;
		  width: 25px;
		  border: 0;
		}
	</style>
</head>
<body>
	<div class="post-embed">
		<p class="embed-heading">
			<a href="{{ route('frontend.post.show', $post->slug) }}" target="_top">{{ $post->title }}</a>
		</p>
		<div class="embed-featured-image square">
			<a href="{{ route('frontend.post.show', $post->slug) }}" target="_top">
				@if($post->featureImage)
					<img src="{{ Helper::getMediaUrl($post->featureImage, 'ratio3x2') }}">
				@endif
			</a>
		</div>
		<div class="embed-excerpt"><p>&nbsp; {!! Helper::getPostSummary($post->content) !!} … <a href="{{ route('frontend.post.show', $post->slug) }}" class="embed-more" target="_top">Đọc thêm</a></p>
		</div>

		
		<div class="embed-footer">
			<div class="embed-site-title">
				<a href="{{ route('frontend.home.index') }}" target="_top">
					@if($post->user->avatar)
						<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}" class="embed-site-icon">
					@endif
					<span>{{ $post->user->name }}</span>
				</a>
			</div>
			
		</div>
	</div>
</body>
</html>