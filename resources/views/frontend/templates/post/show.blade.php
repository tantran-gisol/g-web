@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main article details page content -->

		<div class="main-article-details-page-content">
			@include('frontend.templates.post.parts.content_post')
		</div>

		<!--x-- main article details page content --x-->
		<!-- form load more -->
		<form id="form-load-more" action="{{ route('frontend.post.loadmore') }}" method="POST">
			{{ csrf_field() }}
			<input type="hidden" name="list_post_id[]" value="{{ $post->id }}" />
			@if(count($post->tags))
				@foreach($post->tags as $tag)
					<input type="hidden" name="list_tag_id[]" value="{{ $tag->id }}" />
				@endforeach
			@endif
		</form>
		<!--x-- form load more -->
	</section>

@endsection
@section('header_css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/ckeditor-custom-content.css') }}">
@endsection