@extends('frontend.layouts.app')
@section('content')
<section id="intro" style="height:42vh">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active" style="height:400px;">
            <div class="carousel-background"><img src="{{ asset('image/lienhe.png') }}"></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>LIÊN HỆ</h2>
                <!-- <p>Thành công của tương lai bắt đầu từ hôm nay.</p> -->
                <!-- <a href="#featured-services" class="btn-get-started scrollto">Get Started</a> -->
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
</section>
<main id="main">
    <section id="services">
      <div class="container">

        <div class="row" style="width: 160%;">
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="img">
              <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/vanphongeduzone.jpg" class="attachment-original size-original" style="width: 100%;">
            </div>
          </div>

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <h4 class="title" style="text-align:left;color:#0071bb;font-size:20px;">Trường Đại học quốc tế Mona Media</h4>
            <p class="description" style="text-align:left;font-size: 15px;">319 C16 Lý Thường Kiệt, Phường 15, Quận 11, Tp.HCM</p>
            <p class="description" style="text-align:left;font-size: 15px;">076 922 0162</p>
            <p class="description" style="text-align:left;font-size: 15px;">demonhunterg@gmail.com - mon@mona.media</p>
            <p class="description" style="text-align:left;font-size: 15px;">www.mona.media</p>
            <p class="description" style="text-align:left;font-size: 15px;">Làm việc từ thứ 2 – thứ 6 (9h – 18h)</p>
          </div>
        </div>

        <div class="row" style="width: 160%;">
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div id="map-container-google-2" class="z-depth-1-half map-container">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3835.1579728222896!2d108.21083151514856!3d16.00528994539892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31421a3d9db77c8b%3A0x5cb943bd017b6ede!2zODEgVsSDbiBUaeG6v24gRMWpbmcsIEhvw6AgWHXDom4sIEPhuqltIEzhu4csIMSQw6AgTuG6tW5nIDU1MDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1633421021344!5m2!1svi!2s" frameborder="0" style="border:0;height: 440px;width:100%" allowfullscreen></iframe>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1s">
            <h4 class="title" style="text-align:left;color:#0071bb;font-size:20px;">Đăng ký tư vấn miễn phí:</h4>
            <div class="container mt-5" style="margin-top: 1.5rem !important;">
            <!-- Success message -->
                  @if(Session::has('success'))
                      <div class="alert alert-success">
                          {{Session::get('success')}}
                      </div>
                  @endif
              <form action="{{ route('frontend.lienhe.index') }}" method="post" role="form" class="contactForm" style="margin-left:8%">
              @csrf
                <div class="form-group">
                  <label>Họ Tên</label>
                  <input type="text" class="form-control {{ $errors->has('name') ? 'error' : '' }}" name="name" id="name">
                  @if ($errors->has('name'))
                    <div class="error">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>

                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control {{ $errors->has('email') ? 'error' : '' }}" name="email" id="email">
                  @if ($errors->has('email'))
                    <div class="error">
                      {{ $errors->first('email') }}
                    </div>
                  @endif
                </div>

                <div class="form-group">
                  <label>Tiêu đề</label>
                  <input type="text" class="form-control {{ $errors->has('subject') ? 'error' : '' }}" name="subject" id="subject">
                  @if ($errors-> has('subject'))
                    <div class="errors">
                       {{ $errors->first('subject') }}
                    </div>
                   @endif 
                </div>

                <div class="form-group">
                  <label>Tin nhắn</label>
                  <textarea class="form-control {{ $errors->has('message') ? 'error' : '' }}" name="message" id="message" rows="4"></textarea>

                  @if ($errors->has('message'))
                    <div class="error">
                      {{ $errors->first('message') }}
                    </div>
                  @endif
                </div>
                <button type="submit" class="btn btn-lg btn-block btn-danger toggleResult" id="" style="background-color:rgb(0, 113, 187)">Gửi câu hỏi</button>
            </form>
          </div>
        </div>
      </div>
    </section>

  </main>
@endsection