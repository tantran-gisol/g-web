@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>

        <div class="row about-cols" style="margin-left: 30%">
        <h3 class="section-header">Thực tập sinh Điều dưỡng tại Nhật Bản</h3>            
          <img src="{{ asset('image/about-mission.jpg') }}" alt="" class="img-fluid">
          <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
          <!-- <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2> -->
          <p style="font-size:14px">
            <br>Chương trình Thực tập sinh Điều Dưỡng Nhật Bản hay còn gọi là Xuất khẩu lao động ngành Điều dưỡng tại Nhật Bản là chương trình chính thức có hiệu lực từ ngày 1/11/2017, cho phép người lao động Việt Nam có cơ hội sang làm việc tại trong lĩnh vực Điều dưỡng tại các cơ sở y tế và viện dưỡng lão ở Nhật Bản.<br><br>
            Nhu cầu lao động điều dưỡng của Nhật Bản ngày càng tăng cao, dự kiến từ nay tới năm 2025 nước này sẽ thiếu hụt khoảng 380.000 lao động làm việc trong lĩnh vực điều dưỡng, hộ lý, chăm sóc người cao tuổi. Do đó, Chính phủ Nhật Bản đã phê duyệt dự thảo đưa ngành Điều Dưỡng vào trong danh sách các nhóm ngành được phép tuyển chọn theo diện xuất khẩu lao động (thực tập sinh kỹ năng).<br><br>
            CEO Dịch Vụ tự hào là một trong những doanh nghiệp đầu tiên được Bộ lao động thương Binh và Xã hội cho phép tuyển chọn lao động Việt Nam sang Nhật Bản theo chương trình này. Với cơ sở vật chất và quy trình đào tạo đảm bảo theo tiêu chuẩn Nhật Bản,  CEO Dịch Vụ cam kết giúp người lao động Việt Nam được trau dồi kiến thức cần thiết và khả năng ngôn ngữ tốt nhất để có thể đáp ứng được các yêu cầu tuyển dụng khắt khe từ phía các đối tác Nhật Bản<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            Điều kiện tham gia chương trình<br>
          </p>
          <p style="font-size:14px">
            Bạn hãy giới thiệu về bản thân mình (Trả lời ngắn gọn về tên, tuổi, quê quán, và tốt nghiệp trường gì, tránh trả lời lan man, dài dòng )<br><br>
            Bạn đã học tiếng Nhật được bao lâu rồi ? (Trả lời đúng sự thật )<br><br>
            Gia đình bạn có tất cả mấy người? (Trả lời theo đúng thông tin lý lịch đã kê khai trong hồ sơ)<br><br>
            Người thân của bạn làm nghề gì ?<br><br>
            Bạn có người thân đang sống ở Nhật không ?<br><br>
            Bạn muốn học tại trường đại học nào của Nhật Bản? Và bạn muốn theo học chuyên ngành gì? (Trả lời theo Nguyện vọng đã kê khai trong bộ hồ sơ du học)<br><br>
            Mục đích của bạn khi đu du học Nhật là để làm gì? Sau khi ra trường bạn muốn làm việc ở Nhật Bản không? (Trả lời theo Nguyện vọng đã ghi trong Lý do du học nộp cùng bộ hồ sơ )
          </p>
          <p style="font-weight: bold;font-size:16px"> 
             2. Câu hỏi kiếm tra khả năng tài chính.<br><br>
          </p>
          <p style="font-size:14px">
            Đối với du học tự túc thì tài chính là vấn đề then chốt quyết định việc bạn có được cục XNC cấp tư cách lưu trú du học Nhật Bản hay không. Do vậy , khi được cục XNC hỏi câu hỏi này bạn cần bình tĩnh để trả lời chính xác nhất thông tin từ hồ sơ để tạo dựng niềm tin với cục XNC.<br><br>
            Công việc của bố mẹ/ Người bảo lãnh( NBL) bạn là gì? ( Trả lời đúng với hồ sơ, ngoài ra cũng nên tìm hiểu them về nghề nghiệp của bố mẹ/ NBL bạn để tránh khi trả lời bị vấp )<br><br>
            Mức thu nhập hàng tháng của bố mẹ/ NBL bạn là bao nhiêu?<br><br>
            Số tiền tiết kiệm được của bố mẹ/ NBL bạn là bao nhiêu ? Sổ tiết kiệm của bố mẹ bạn mở ở ngân hàng nào ? Tại sao lại mở ở ngân hàng đó ?<br><br>
            Những câu hỏi này hoàn toàn dựa trên hồ sơ của bạn vì thế bạn nên đọc và nhớ kỹ những điều đã viết trong hồ sơ, tránh trường hợp thông tin khi trả lời không chính xác, khiến cho cục XNC nghi ngờ.m
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            3. Câu hỏi về mục đích du học Nhật Bản.<br><br>
          </p>
          <p style="font-size:14px">
            Sở dĩ Cục XNC có những câu hỏi về mục đích du học vì họ muốn kiểm tra học sinh thực chất có phải là du học hay muốn sang để đi làm kiếm tiền. Nên bạn hãy bình tĩnh trả lời để cục XNC thấy rằng mong muốn đi du học của mình là thật sự. Các câu trả lời này mình sẽ trả lời căn cứ trên lý do du học của mình nhé.<br><br>
            Các câu hỏi thường thấy<br><br>
            Tại sao bạn không học tại Việt Nam ?<br><br>
            Có rất nhiều các quốc gia trên thế giới, tại sao bạn bạn lại chọn du học Nhật ?<br><br>
            Bạn có làm thêm khi đi du học tại Nhật không ? (Hãy nói em phải cố gắng học tiếng Nhật thật tốt đã, sau đó em sẽ đi làm thêm, nhưng em chỉ đi làm để lấy kinh nghiệm, nâng cao trình độ tiếng Nhật, …)<br><br>
          </p>                    
        </div>
        <div class="form">
          <div id="sendmessage" style="font-weight:bold;">Email của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><input type="submit"  value="Subscribe" style="background: #28a745;border: 0;width: 35%;padding: 6px 0;text-align: center;color: #fff;transition: 0.3s;cursor: pointer;"></div>
          </form>
        </div>
      </div>
    </section>
  </main>
@endsection