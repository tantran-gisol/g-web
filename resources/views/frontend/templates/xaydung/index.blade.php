@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>

        <div class="row about-cols" style="margin-left: 30%">
          <h3 class="section-header">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</h3>            
          <img src="{{ asset('image/xay-dung.jpg') }}" alt="" class="img-fluid">
          <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
          <!-- <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2> -->
          <p style="font-weight: bold;font-size:16px"> 
            Cơ hội việc làm tại Nhật Bản ngành xây dựng
          </p>
          <p style="font-size:14px">
            Tại Nhật, nhu cầu tiếp nhận lao động nước ngoài trong lĩnh vực xây dựng của Nhật Bản sẽ tăng lên trong thời gian tới để chuẩn bị cho Thế vận hội Tokyo được tổ chức vào năm 2020. Cụ thể, kỹ thuật viên và kỹ sư ngành xây dựng tại Nhật đang cần khoảng trên 10 nghìn lao động có tay nghề tốt; thực tập sinh đã hoàn thành 3 năm thực tập trong ngành xây dựng về nước sẽ được quay lại Nhật làm việc theo hợp đồng 2 năm nếu thời gian về nước dưới 1 năm và được quay lại làm việc theo hợp đồng 3 năm nếu thời gian về nước từ 1 năm trở lên.
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            Nguồn nhân lực ngành xây dựng ở Nhật<br>
          </p>
          <p style="font-size:14px">
            Các bạn thấy nếu tham gia theo diện tu nghiệp sinh hay thực tập sinh thì thực chất là dịch vụ dành cho lao động phổ thông. Riêng lao động có tay nghề tốt, có trình độ cao đẳng trở lên thuộc một trong các nhóm ngành xây dựng thì các bạn có thể đi theo diện kỹ thuật viên hoặc kỹ sư. Diện này có nhiều ưu điểm nổi bật: lương cao; chi phí đi rất thấp và không cần đặt cọc; không bị giới hạn thời gian ở lại Nhật như diện thực tập sinh kỹ năng. Do đó đòi hỏi quan trọng đối với diện này là phải có trình độ tiếng Nhật đạt tối thiếu N3 trước khi thi tuyển. Nếu bạn chọn học ngành xây dựng ở Việt Nam thì rõ ràng sẽ phải đầu tư học tiếng Nhật ít nhất 1 năm nữa mới có thể đi được Nhật theo diện kỹ sư. Cách khác, đó là bạn sang Nhật du học chuyên ngành xây dựng để dễ dàng hơn khi tìm kiếm cơ hội việc làm.
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            Học ngành xây dựng tại Nhật Bản<br>
          </p>
          <p style="font-size:14px">
           Nói đến du học ở Nhật cái được không phải chỉ riêng cơ hội việc làm. Mà lớn hơn, đó là tư duy, tầm nhìn của bạn sẽ thay đổi tốt hơn bên cạnh đó bạn lĩnh hội được nhiều kiến thức, kỹ năng ưu điểm của người Nhật, của văn hóa Nhật.
          </p>                    
        </div>
        <div class="form">
          <div id="sendmessage" style="font-weight:bold;">Email của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><input type="submit"  value="Subscribe" style="background: #28a745;border: 0;width: 35%;padding: 6px 0;text-align: center;color: #fff;transition: 0.3s;cursor: pointer;"></div>
          </form>
        </div>
      </div>
    </section>
  </main>
@endsection