@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>

        <div class="row about-cols" style="margin-left: 30%">
          <h3 class="section-header">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</h3>            
          <img src="{{ asset('image/sushi.jpg') }}" alt="" class="img-fluid">
          <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
          <!-- <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2> -->
          <p style="font-weight: bold;font-size:16px"> 
            1.Tại sao nên đi du học Nhật Bản ngành nấu ăn
          </p>
          <p style="font-size:14px">
            Sự nổi tiếng của ẩm thực Nhật Bản trên thế giới thì không thể phủ nhận. Chính bởi sự trang trí tinh tế, các món ăn đang mang lại nhiều dinh dưỡng. Vậy để làm sao có được các món ăn vừa có chất lượng vừa có hình thức đẹp. Tất cả những bí kíp này bạn sẽ hoàn toàn được học hỏi khi tham gia đi du học Nhật Bản ngành nấu ăn<br><br>
            Du học Nhật ngành ẩm thực sẽ giúp bạn trau dồi được các kỹ năng. Thêm nữa, đây thuộc một trong những ngành du học Nhật Bản có nguồn nhân lực khan hiếm. Vì thế, bạn có thể dễ dàng xin được việc làm đúng chuyên ngành mà lại mang lại thu nhập ổn định. Điển hình như các bạn sinh viên ra trường có thể xin vào các nhà hàng, quán ăn, tiệm ăn uống …. Thậm chí, bạn cũng có thể mở nhà hàng riêng.<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            2.Các trường đào tạo du học Nhật Bản ngành ẩm thực<br>
          </p>
          <p style="font-size:14px">
            Bạn muốn du học Nhật Bản ngành ẩm thực, bạn chưa biết chọn trường như thế nào? Hãy tham khảo những ngồi trường có danh tiếng về trình độ giảng dạy ngành nấu ăn này nhé<br><br>
            So với các trường khác, đây được mệnh danh là “kinh đô” giảng dạy ngành ẩm thực cao cấp ở Nhật. Nếu so sánh về chất lượng đào tạo đầu bếp, ngôi trường này là nơi giảng dạy hàng đầu tại Nhật Bản. Hơn thế nữa, học viện Ẩm thực Tsuji còn đứng top 3 trong số những trung tâm đào tạo đầu bếp lớn nhất so với thế giới.<br><br>
            Nếu so sánh các trường nấu ăn có tiếng nhất ở Tokyo mà bỏ qua Tokyo Belle Epoque thì quả là thiếu sót lớn. Trường có đầy đủ các khoa cho bạn lựa chọn như Khoa làm bánh, Khoa nấu ăn, Khoa kinh doanh café…. Trong đó, du học ngành làm bánh tại Nhật Bản là nổi tiếng hơn so với những ngành còn lại.<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            3.Một vài câu hỏi liên quan đến du học Nhật Bản ngành ẩm thực<br>
          </p>
          <p style="font-size:14px">
           Để so sánh và lựa chọn được ngành học và trường học phù hợp là điều không dễ dàng. Liên quan đến vấn đề du học Nhật Bản ngành ẩm thực, đã có không ít banjc ó những thắc mắc như:<br><br>
           Senmon nấu ăn ở đây chính là tên gọi của trường dạy nghề nấu ăn, ẩm thực ở Nhật Bản. Nếu so về ưu điểm, học Senmon nấu ăn có thời gian học khá ngắn, điều này giúp bạn tiết kiệm được thời gian học, bạn chỉ cần mất 1-3 năm, chứ không phải là 2 năm để ra trường.<br><br>
            Một trong những điều kỳ diệu của ẩm thực Nhật đó là những chiếc bánh tinh tế. Du học ngành làm bánh tại Nhật Bản được coi là một trong những ngành tốt nhất ở Nhật trong lĩnh vực ẩm thực. Lượng sinh viên Việt du học Nhật ngành ẩm thực mà theo học nghề làm bánh rất đông.
          </p>                    
        </div>
        <div class="form">
          <div id="sendmessage" style="font-weight:bold;">Email của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><input type="submit"  value="Subscribe" style="background: #28a745;border: 0;width: 35%;padding: 6px 0;text-align: center;color: #fff;transition: 0.3s;cursor: pointer;"></div>
          </form>
        </div>
      </div>
    </section>
  </main>
@endsection