@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>

        <div class="row about-cols" style="margin-left: 30%">
          <h3 class="section-header">Du học Nhật Bản ngành công nghệ thông tin năm 2018 – 2019</h3>            
          <img src="{{ asset('image/cntt.jpg') }}" alt="" class="img-fluid">
          <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
          <!-- <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2> -->
          <p style="font-size:15px;font-weight: bold;">
            <br>Đi du học Nhật Bản ngành công nghệ thông tin năm 2018 – 2019 ở tại Hà Nội. Các trường đại học, viện công nghệ TOKYO, kỹ sư làm việc tại Nhật, học bổng…<br>
          </p>
          <p style="font-size:14px"> 
            Trong cuộc sống hiện đại, công nghệ thông tin đang ngày càng chứng tỏ sức lan truyền rộng rãi của mình. Hầu hết mọi thứ hàng ngày của chúng ta đều có liên quan tới công nghệ thông tin. Trong khi đó, nguồn nhân lực được đào tạo bài bản về ngành này thì lại đang trong tình trạng khan hiếm trầm trọng. Chính vì thế, du học ngành công nghệ thông tin tại Nhật Bản đang trở thành một xu thế mới, được nhiều học sinh lựa chọn để theo đuổi.<br>
          </p>
          <p style="font-size:15px;font-weight: bold;">
            Vì sao nên học ngành công nghệ thông tin khi sang du học Nhật Bản?<br>
          </p>
          <p style="font-size:14px"> 
             Thứ nhất, khi nhắc tới đất nước Nhật Bản, chúng ta nhắc tới một cường quốc vững mạnh về công nghệ thông tin, với những sáng tạo nổi trội đi đầu thế giới. Không khó để có thể kể tên một số thương hiệu quen thuộc và được ưa dùng khắp mọi nơi như: Sony, Panasonic, Toshiba, Hitachi, Fujitsu… Hơn thế nữa, Nhật Bản còn được biết đến với vô số phần mềm điện tử,  máy tính, công nghệ cao, vô cùng hữu ích và được nhiều người ưa dùng. Chính những điều đó đã tạo ra sức hút cực mạnh đối với ngành công nghệ thông tin khi các bạn học sinh đi du học Nhật Bản.<br><br>
             Đi du học Nhật Bản ngành công nghệ thông tin năm 2018 – 2019 ở tại Hà Nội<br>

            Không phải ngẫu nhiên mà Nhật Bản đạt được những thành công đáng chú ý như vậy. Những thành quả đó là do chính phủ Nhật Bản đã ra sức đầu tư vào đào tạo nhân lực cho công nghệ thông tin. Quốc gia này từng bỏ ra tới 300 triệu USD để phổ cập tin học cho tất cả các trường học, và luôn luôn nâng cấp trang thiết bị dạy học môn tin học trong các nhà trường nhằm tạo ra một môi trường chuyên nghiệp và hiện đại nhất cho học sinh phát triển kỹ năng của mình. Chính vì vậy mà các trường chuyên về đào tạo công nghệ thông tin ở xứ sở hoa anh đào luôn được đánh giá là có chất lượng tốt và là cái nôi đào tạo ra các thiên tài sáng chế cho cả thế giới.<br><br>

            Sau các giờ học lý thuyết trên lớp, các sinh viên IT sẽ được trải nghiệm thực tế, được áp dụng những gì vừa mới học được ở tại các công ty hàng đầu Nhật Bản. Đó là cơ hội để các bạn thỏa sức thể hiện sức sáng tạo, tò mò của bản thân, đồng thời cũng là cơ hội để học tập kinh nghiệm từ các “não làng” trong ngành. Kiểu học lý thuyết song song với thực hành này vô cùng hiệu quả và đã giúp vô số sinh viên trở lên ngày một “lành nghề” hơn.<br><br>

            Kinh nghiệm học tập ngành công nghệ thông tin ở Nhật Bản là gì?<br><br>

            Như chúng ta đã biết, công nghệ thông tin là ngành đòi hỏi nhiều sự sáng tạo nhất. Các bạn sinh viên phải luôn luôn tìm tòi, khám phá và thật năng động thì mới có thể gặt hái được nhiều thành công từ ngành này. Các sinh viên IT lúc nào cũng phải thật nỗ lực, có óc quan sát thật kỹ lưỡng và một bộ não hoạt động nhanh nhạy bởi vì công nghệ thông tin là ngành hết sức đa dạng, và được áp dụng cho tất cả mọi lĩnh vực của đời sống. Trong thời buổi công nghệ phát triển cuồn cuộn như một cơn lốc xoáy, nếu chúng ta không biết cách tự hòa mình vào thì ắt chúng ta sẽ bị đá ra khỏi vòng xoáy đó.<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            3. Câu hỏi về mục đích du học Nhật Bản.<br><br>
          </p>
          <p style="font-size:14px">
            Như đã nói ở trên thì công nghệ thông tin hứa hẹn sẽ mang đến cho bạn những cơ hội việc làm triển vọng với mức lương hậu hĩnh. Có kiến thức về công nghệ thông tin trong tay thì các bạn sẽ đúng như câu nói “vứt ở đâu cũng sống”, một bầu trời rộng mở đang chờ đón các bạn. Theo thống kê thì cứ mỗi năm, nhu cầu nhân lực trong ngành công nghệ thông tin tăng 13%. Hơn thế nữa, công nghệ thông tin lại là một ngành rất ổn định vì không chịu nhiều ảnh hưởng của suy thoái hay khủng hoảng kinh tế, nên việc làm cho lao động cũng hết sức ổn định.
          </p>                    
        </div>
        <div class="form">
          <div id="sendmessage" style="font-weight:bold;">Email của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><input type="submit"  value="Subscribe" style="background: #28a745;border: 0;width: 35%;padding: 6px 0;text-align: center;color: #fff;transition: 0.3s;cursor: pointer;"></div>
          </form>
        </div>
      </div>
    </section>
  </main>
@endsection