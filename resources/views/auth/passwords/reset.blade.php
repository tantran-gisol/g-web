@extends('auth.layouts.app')

@section('content')
    <form method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="admin-login-input col-12">
            <input id="email" type="email"  placeholder="{{ __('メールアドレス') }}"  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" readonly />
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="admin-login-pass col-12">
            <input type="password" placeholder="{{ __('バスワード') }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" autofocus />
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-12">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" />
        </div>
        <div class="col-12 text-center">
            <button class="btn btn-no-radius admin-login-btn w-100">{{ __('パスワードをリセット') }}</button>
        </div>
    </form>
    @if (Route::has('login'))
        <div class="col-12 text-center">
            <label class="forget-txt">
                <a href="{{ route('login') }}">{{ __('ログイン') }}</a>
            </label>
        </div>
    @endif
@endsection
