<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Frontend')->group(function(){
  //HomeController
  Route::get('/', 'HomeController@index')->name('frontend.home.index');
  Route::get('tintuc', 'TintucController@index')->name('frontend.tintuc.index');
  Route::get('lienhe', 'LienheController@index')->name('frontend.lienhe.index');
  Route::post('lienhe', 'LienheController@Lienhe')->name('frontend.lienhe.index');

  // Route::get('dangnhap', 'DangnhapController@index')->name('frontend.dangnhap.index');
  // Route::post('dangnhap', 'DangnhapController@Dangnhap')->name('frontend.dangnhap.index');

  Route::get('gioithieu', 'GioithieuController@index')->name('frontend.gioithieu.index');
  Route::get('danhsach', 'DanhsachController@index')->name('frontend.danhsach.index');
  Route::get('tuyensinh', 'TuyensinhController@index')->name('frontend.tuyensinh.index');
  Route::get('dieuduong', 'DieuduongController@index')->name('frontend.dieuduong.index');
  Route::get('cntt', 'CnttController@index')->name('frontend.cntt.index');
  Route::get('nhks', 'NhksController@index')->name('frontend.nhks.index');
  Route::get('kythuat', 'KythuatController@index')->name('frontend.kythuat.index');
  Route::get('daubep', 'DaubepController@index')->name('frontend.daubep.index');
  Route::get('xaydung', 'XaydungController@index')->name('frontend.xaydung.index');
  Route::get('chitiet', 'ChitietController@index')->name('frontend.chitiet.index');
  Route::get('lichhoc', 'LichhocController@index')->name('frontend.lichhoc.index');

  // Route::get('loginuser', 'LoginuserController@index')->name('frontend.loginuser.index');
  //SearchController
  /*Route::get('search', 'SearchController@index')->name('frontend.search.index');
  Route::get('search/result', 'SearchController@result')->name('frontend.search.result');*/
  //PostController
  Route::get('{slug}', 'PostController@show')->name('frontend.post.show');
  Route::get('{slug}/album', 'PostController@album')->name('frontend.post.album');
  Route::prefix('blog')->group(function(){
    Route::post('submit-vote/{id}', 'PostController@submitVote')->name('frontend.post.submitVote');
    Route::middleware(['auth'])->group(function(){
    	Route::get('preview/{id}', 'PostController@preview')->name('frontend.post.preview');
    });
    Route::post('load-more', 'PostController@loadMore')->name('frontend.post.loadmore');
  });
  //UserController
  Route::prefix('user')->group(function(){
    Route::get('{id}', 'UserController@show')->name('frontend.user.show');
  });
  //CategoryController
  Route::prefix('cat')->group(function(){
    Route::get('{id}', 'CategoryController@show')->name('frontend.category.show');
  });
  //TagController
  Route::prefix('tag')->group(function(){
    Route::get('feature', 'TagController@featureIndex')->name('frontend.tag.feature');
    Route::get('series', 'TagController@seriesIndex')->name('frontend.tag.series');
    Route::get('{id}', 'TagController@show')->name('frontend.tag.show');
  });
  //PageController
  Route::prefix('_p')->group(function(){
    /*Route::get('term-of-service', 'PageController@termOfService')->name('frontend.page.termofservice');
    Route::get('privacy-policy', 'PageController@privacyPolicy')->name('frontend.page.privacypolicy');*/
    //InquiryController
    Route::get('inquiry', 'InquiryController@index')->name('frontend.inquiry.index');
    Route::post('inquiry', 'InquiryController@submit')->name('frontend.inquiry.submit');
    //Search page
    Route::get('search', 'SearchController@index')->name('frontend.search.index');
    Route::get('search/result', 'SearchController@result')->name('frontend.search.result');
    //Normal page
    Route::get('{slug}', 'PageController@show')->name('frontend.page.show');
  });
  //Widget Embed
  Route::get('widget/embed', 'WidgetController@embed')->name('frontend.widget.embed');
});