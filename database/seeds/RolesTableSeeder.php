<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::count() == 0) {

            Role::create([
                'slug'          => 'admin',
                'name'           => 'Admin'
            ]);

            Role::create([
                'slug'          => 'writer',
                'name'           => 'Writer'
            ]);
        }
    }
}
